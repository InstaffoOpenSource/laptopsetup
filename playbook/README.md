# laptopSetupPlaybook

Set up a local development environment.

## Initial setup

1. Install requirements
   * `ansible-galaxy install --force -r requirements.yml -p roles/`
2. Ping host
   * `ansible all -i hosts -m ping`
3. (On first run only!) Update all packages
   * `ansible all -i hosts -m apt -a "upgrade=yes update_cache=yes" --become`
4. (On first run only!) Reboot machine
   * `ansible all -i hosts -m reboot --become`
5. Run the playbook
   * `ansible-playbook -i hosts playbook.yml`
