#!/usr/bin/env bash

set -ueo

# Function declarations

function add_to_PATH_and_source() {
  echo 'export PATH="$HOME/'$1':$PATH"' >> $HOME/.bashrc && . $HOME/.bashrc
  export PATH="$HOME/$1:$PATH"
}

function print_in_green() {
  echo -e "\e[32m============ $1 ==============\e[0m"
}

function install_python_packages() {
  print_in_green "Installing dependencies"
  sudo apt install -y ssh-askpass     \
    auditd                            \
    make                              \
    build-essential                   \
    libssl-dev                        \
    zlib1g-dev                        \
    libbz2-dev                        \
    libreadline-dev                   \
    libsqlite3-dev                    \
    wget                              \
    curl                              \
    llvm                              \
    libncurses5-dev                   \
    libncursesw5-dev                  \
    xz-utils                          \
    tk-dev                            \
    libffi-dev                        \
    liblzma-dev                       \
    python3-openssl                   \
    git                               \
    python-wheel                      \
    python-dev

  print_in_green "Install pyenv"
  curl -L https://raw.githubusercontent.com/pyenv/pyenv-installer/master/bin/pyenv-installer | bash
  add_to_PATH_and_source .pyenv/bin

  print_in_green "Install python 3.7.3 globally"
  pyenv install 3.7.3
  eval "$(pyenv init -)"
  pyenv global 3.7.3
  pyenv rehash

  print_in_green "Install poetry"
  curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python
  add_to_PATH_and_source .poetry/bin
  poetry config repositories.instaffo https://pypi.instaffo.tech

  print_in_green "Install pip"
  sudo apt install python-pip -y
  pip install --upgrade pip
  add_to_PATH_and_source .local/bin

  print_in_green "Install pip packages"
  pip install --upgrade setuptools    	\
			cookiecutter	\
			jupyterlab	\
			jupytext	\
			nbconflux 	\
			requests 	\
			html5lib	\
			papermill
  add_to_PATH_and_source .pyenv/versions/3.7.3/bin

  print_in_green "Install jupyter packages"
  sudo chown -R $USER:$(id -gn $USER) $HOME/.config
  jupyter labextension install @jupyter-widgets/jupyterlab-manager
  jupyter labextension install @jupyterlab/toc
  jupyter labextension install @jupyterlab/celltags

  print_in_green "Python packages installed"
}

function default_setup() {
  sudo apt-get update
  sudo apt-get -y upgrade

  sudo apt-get install -y vim curl


  # install cool stuff
  sudo apt-get install -y git

  # install docker
  print_in_green "Install docker"
  sudo apt-get install -y apt-transport-https \
        ca-certificates                       \
        curl                                  \
        gnupg-agent                           \
        software-properties-common

  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
  sudo apt-get install -y docker-ce docker-ce-cli containerd.io
  sudo usermod -aG docker $USER

  # install docker-compose
  print_in_green "Install docker-compose"
  sudo apt install -y docker-compose


  # install rbenv
  print_in_green "Install rbenv"
  sudo apt-get install -y autoconf \
        bison                      \
        build-essential            \
        libssl-dev                 \
        libyaml-dev                \
        libreadline-dev            \
        zlib1g-dev                 \
        libncurses5-dev            \
        libffi-dev                 \
        libgdbm5                   \
        libgdbm-dev
  git clone https://github.com/rbenv/rbenv.git ~/.rbenv
  git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build

  echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> $HOME/.bashrc
  echo 'eval "$(rbenv init -)"' >> $HOME/.bashrc
  export PATH="$HOME/.rbenv/bin:$PATH"
  . $HOME/.bashrc

  # install cool ruby stuff
  rbenv install 2.5.1
  rbenv global 2.5.1
  add_to_PATH_and_source .rbenv/shims

  gem install bundler
  gem install inspec

  # install node
  print_in_green "Install node"
  curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
  sudo apt update && sudo apt-get install -y nodejs
  mkdir "${HOME}/.npm-packages"
  npm config set prefix "${HOME}/.npm-packages"
  echo 'NPM_PACKAGES="${HOME}/.npm-packages"' >> $HOME/.bashrc
  echo 'export PATH="$PATH:$NPM_PACKAGES/bin"' >> $HOME/.bashrc
  NPM_PACKAGES="${HOME}/.npm-packages"
  export PATH="$PATH:$NPM_PACKAGES/bin"
  npm install npm@latest -g
  sudo chown -R $USER:$(id -gn $USER) $HOME/.config

  # install yarn
  print_in_green "Install yarn"
  curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
  echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
  sudo apt-get update
  sudo apt-get install -y yarn

  # install mysql client
  print_in_green "Install mysql-client"
  sudo apt install -y mysql-client

  # some cool kernel stuff that is needed
  echo "vm.max_map_count=262144\nfs.inotify.max_user_watches=524288" | sudo tee -a /etc/sysctl.conf
  sudo sysctl -p

}


# Variable declarations

OPTION1='Default - Installs git, docker, docker-compose, rbenv, nodejs, yarn, mysql-client a.o.'
OPTION2='Python  - Installs Default + pyenv, poetry, pip, jupyterlab, cookiecutter a.o.'


# Script Start

clear

print_in_green "Welcome at Instaffo!!"
PS3='Please enter your choice: '
options=("${OPTION1}" "${OPTION2}" "Quit")
select opt in "${options[@]}"
do
  case $opt in
    $OPTION1)
      print_in_green "Starting Default Setup"
      sleep 2
      default_setup
      break
      ;;
    $OPTION2)
      print_in_green "Starting Default Setup + Python Package Setup"
      sleep 2
      default_setup && install_python_packages
      break
      ;;
    "Quit")
      echo -e "\033[31;5mNo packages were installed - Script will exit\033[0m"
      exit 1
      ;;
    *) echo "invalid option $REPLY";;
  esac
done

echo
echo -e "\033[33;5mPlease login again on your system\033[0m"
