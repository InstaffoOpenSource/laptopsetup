FROM ubuntu:18.04

RUN useradd -ms /bin/bash testuser
ENV USER testuser
ENV HOME /home/testuser
COPY ./setup_ubuntu.sh /home/testuser/.

RUN apt-get update && apt-get install -y \
  gpg  \
  vim  \
  curl \
  sudo \
  tzdata \
  debconf

RUN echo "testuser    ALL=(ALL:ALL) NOPASSWD: ALL" >> /etc/sudoers

USER testuser
WORKDIR /home/testuser
ENTRYPOINT ["/bin/bash"]
